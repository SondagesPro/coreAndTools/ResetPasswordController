<?php
/**
 * A controller to set your own password
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class ResetPasswordController extends LSYii_Controller {

  function actionindex(){
    $userName=flattenText(App()->getConfig('resetusername'), false, true);
    $userPassword=App()->getConfig('resetpassword');
    if(!$userName || !$userPassword){
      Yii::app()->setFlashMessage("No config set",'error');
      $this->redirect("admin");
    }
    if(User::model()->count("users_name=:users_name and uid!=1",array(':users_name'=>$userName))){
      Yii::app()->setFlashMessage("Username already exist",'error');
      $this->redirect("admin");
    }
    $superAdminUser=User::model()->findByPk(1);
    if(!$superAdminUser) {
      Yii::app()->setFlashMessage("Initial super admin didn't exist. This controller need initial super admin.",'error');
      $this->redirect("admin");
    }
    $superAdminUser->users_name= $userName;
    if(is_callable(array($superAdminUser,'setPassword'))) {
      $superAdminUser->setPassword($userPassword);
    } else {
      $superAdminUser->password= hash('sha256', $userPassword);
    }
    if($superAdminUser->save()) {
      Yii::app()->setFlashMessage("Password and Username updated",'success');
      Yii::app()->setFlashMessage("Remind to remove this file or to remove config part",'warning');
    } else {
      Yii::app()->setFlashMessage("Unknow error happen",'error');
    }
    $this->redirect("admin");
  }
}

