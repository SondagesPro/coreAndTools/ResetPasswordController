# ResetPasswordController

Reset easily username and password of initial super admin with only FTP access.

## Installation and usage

Put this file in `./application/controllers/` directory of your limesurvey installation.

Update your `./application/config/config.php` file to add `resetusername` and `resetpassword` in the config array.


        'config'=>array(
        // debug: Set this to 1 if you are looking for errors. If you still get no errors after enabling this
        // then please check your error-logs - either in your hosting provider admin panel or in some /logs directory
        // on your webspace.
        // LimeSurvey developers: Set this to 2 to additionally display STRICT PHP error messages and get full access to standard templates
            'debug'=>2,
            'debugsql'=>0, // Set this to 1 to enanble sql logging, only active when debug = 2
            // Update default LimeSurvey config here
            'resetusername'=>'The new user name (login)',
            'resetpassword'=>'The new password',
        )


Go to ResetPassword controller via your browser : can be `example.org/index.php/ResetPassword` or `example.org/index.php?r=ResetPassword`.
If no error are shown you can log in with the new user name and the new password.

**Remind to remove this file or the config part after reset are done.**
